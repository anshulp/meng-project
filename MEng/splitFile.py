##################Making Training and Test Based on the fold##############################################

import sys
def splitFile(fullfilename,numberOfFold,model):
    print fullfilename,numberOfFold,model
    fullfile=open(fullfilename,'r')

    ######Separating positive and negative####################################################

    c=0

    positivefile=open('../data/'+str(model)+'-SVMPositive.txt','w')
    negativefile=open('../data/'+str(model)+'-SVMNegative.txt','w')

    lines=fullfile.readlines()
    lengthOfFile=len(lines)
    for line in lines:
        if c<(lengthOfFile/2):
            positivefile.write(line)
            c=c+1
        else:
            negativefile.write(line)
            c=c+1
        
    positivefile.close()
    negativefile.close()

    ################Separating into folds#################################################

    positivefile=open('../data/'+str(model)+'-SVMPositive.txt','r')
    negativefile=open('../data/'+str(model)+'-SVMNegative.txt','r')

    for counter in range(5):
        innercount=0
        trainfile=open('../data/'+str(model)+'-SVM.txt'+str(counter),'w')
        for line in positivefile:
            trainfile.write(line)
            innercount+=1
            if innercount==((lengthOfFile/2)/numberOfFold):
                trainfile.close()
                break

    for counter in range(numberOfFold):
        innercount=0
        trainfile=open('../data/'+str(model)+'-SVM.txt'+str(counter),'a')
        for line in negativefile:
            trainfile.write(line)
            innercount+=1
            if innercount==((lengthOfFile/2)/numberOfFold):
                trainfile.close()
                break

splitFile('../data/'+str(sys.argv[1])+'-SVMFile',int(sys.argv[2]),sys.argv[1])

print 'done splitting'

    


