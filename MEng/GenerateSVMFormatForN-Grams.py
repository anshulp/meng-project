import os, re, random


tokenList = []
dsnames = ['mturk', 'true']

def findAllTokens_old(fname):
    
    line = str(open(fname, 'r').readline())
    
    tokens = line.lower().split(" ")

    for i in range(0, len(tokens)):
        token = str(tokens[i])
        key = token.split("/")
        
        try:
            s = ""
            #to take care of 170/night as a single word
            if len(key) > 2:
                i = 0
                while i < len(key)-1:
                    s += key[i]+"/"
                    i += 1
                #remove the last "/"
                s = s[:len(s)-1]
            else:
                s = str(key[0])
            s = s.strip()
            if tokenList.__contains__(s):
                continue
            else:
                tokenList.append(s)
        except:
            continue
        




def getTokenList():
    
    docs = open(path+"/true/docs-till-now.txt").readlines()
    
    for dsname in dsnames:
        for doc in docs:
            dir = path + dsname + "/" + doc + "/"
            for filename in os.listdir(dir):
                if filename.endswith('.txt'):
                    fname = dir + filename
                    findAllTokens(fname)




def getLine(fname, tokenList):
    line = str(open(fname, 'r').readline())
    newLine = ""
    i = 0
    for item in tokenList[:]:
        
        token = str(tokenList[i])
#        print token
        if token == "+":
            token = "/+"
        elif token == "?":
            token = "/?"
        l = len(re.findall(token, line.lower(), 0))
        newLine += str(i+1)+":"+str(l)+" "
        i = i+1
   
    return newLine.strip() 

               

def createDatasetForSVMLight(tokenList):
    
    newFile = open(path+"experiments/NGram-SVMFile", "a")
    
    docs = open(path+"/true/docs-till-now.txt").readlines()
    
    for dsname in dsnames:
        for doc in docs:
            dir = path + dsname + "/" + doc + "/"
            for filename in os.listdir(dir):
                if filename.endswith('.txt'):
                    fname = dir + filename
                    newLine = getLine(fname, tokenList)
                    if filename.startswith('t_'):
                        newFile.write("+1 "+newLine)
                    else:
                        newFile.write("-1 "+newLine)
                    newFile.write("\n")
    newFile.close()



def randomlyCreateTrainAndTestSVMFiles():
    
    lines = open(path+"experiments/NGram-SVMFile").readlines()
    
    f =  open(path+"experiments/NGram-SVMFile")
   
    trainLines = []
    
    writeTrainFile = open(path+"experiments/NGram-dataSVM.train", "w")
    i = 0
    while i < 650:
        j = random.randint(0, 799)
        if trainLines.__contains__(j):
            continue
        trainLines.append(j)
        
        writeTrainFile.write(lines[j])
        
        i = i+1
        
    writeTrainFile.close()
    
    writeTestFile = open(path+"experiments/NGram-dataSVM.test", "w")    
    
    i = 0
    while i < 800:
        if trainLines.__contains__(i):
            i = i+1
            continue
        writeTestFile.write(lines[i])
        i = i + 1
        
    writeTestFile.close()
        
               
          

def __main__():
    
    getTokenList()
    print tokenList, len(tokenList)
    
    if os.path.exists(path+"experiments/NGram-SVMFile"):
        os.remove(path+"experiments/NGram-SVMFile")
    if os.path.exists(path+"experiments/NGram-dataSVM.train"):
        os.remove(path+"experiments/NGram-dataSVM.train")
    if os.path.exists(path+"experiments/NGram-SdataSVM.test"):
        os.remove(path+"experiments/NGram-ataSVM.test")
        
    createDatasetForSVMLight(tokenList)
    
#    randomlyCreateTrainAndTestSVMFiles()    
    print "SVM Train and Test Files for N-Gram data are Created"
        


        
__main__()

