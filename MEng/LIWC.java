import java.io.*;
import java.util.*;
import java.util.Map.Entry;
import java.util.regex.*;


public class LIWC
{
    private PrintStream out = System.out;
    private InputStream in = System.in;
    private InputStream dic = null;
    private HashMap<Integer, String> descriptions = new HashMap<Integer, String>();
    private Dictionary dictionary = new Dictionary();
    private HashMap<String, int[]> unknowns = new HashMap<String, int[]>();
    private HashMap<Integer, int[]> counts = new HashMap<Integer, int[]>();
    private int total = 0;

    public static void main(String[] args)
    {
        try
        {
            LIWC app = new LIWC(args);
            //app.inlineTag();
            app.calc();
            app.output();
        }
        catch(IllegalArgumentException e)
        {
            System.out.println("Usage: java LIWC -in <input file> -out <output file> -dic <dictionary>");
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
    
    private LIWC(String[] args) throws Exception
    {
        if(args.length % 2 != 0)
        {
            throw new IllegalArgumentException();
        }
        for(int i = 0; i < args.length; i += 2)
        {
            if(args[i].equals("-in"))
            {
                in = new FileInputStream(args[i + 1]);
            }
            else if(args[i].equals("-out"))
            {
                out = new PrintStream(new FileOutputStream(args[i + 1]), true);
            }
            else if(args[i].equals("-dic"))
            {
                dic = new FileInputStream(args[i + 1]);
            }
            else
            {
                throw new IllegalArgumentException();
            }
        }
        if(dic == null)
        {
            throw new IllegalArgumentException();
        }
        readInDictionary();
    }
    
    private void readInDictionary() throws Exception
    {
        BufferedReader br = new BufferedReader(new InputStreamReader(dic));
        String line;
        int stage = -1;      // 0 for catagory, 1 for words
        int lineno = 0;
        Pattern linePattern = Pattern.compile("[^\\s\\(<][^\\s]*|<[^>]*>[^\\s]+|\\([^\\)]*\\)[^\\s]+");
        while((line = br.readLine()) != null)
        {
            ++lineno;
            line = line.trim();
            if(line.length() > 0)
            {
                if(line.startsWith("%"))
                {
                    ++stage;
                }
                else if(stage == 0)
                {
                    String[] words = line.split("\\s+");
                    if(words.length != 2)
                    {
                        throw new Exception("Unexpected format in dictionary: line " + lineno);
                    }

                    int cat = Integer.valueOf(words[0]);
                    descriptions.put(cat, words[1]);
                }
                else if(stage == 1)
                {
                    Matcher m = linePattern.matcher(line);
                    int idx = 0;
                    String word = null;
                    LinkedList<Rule> rules = new LinkedList<Rule>();
                    while(m.find())
                    {
                        String str = m.group();
                        if(idx == 0)
                        {
                            word = str;
                        }
                        else
                        {
                            rules.add(new Rule(str));
                        }
                        ++idx;
                    }
                    if(idx < 2)
                    {
                        throw new Exception("Unexpected format in dictionary: line " + lineno);
                    }
                    
                    dictionary.put(word, rules);
                }
            }
        }
    }
    
    private void inlineTag() throws Exception
    {
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        String line;
        while((line = br.readLine()) != null)
        {
            StringReader sr = new StringReader(line);
            
            StreamTokenizer st = new StreamTokenizer(sr);
            st.resetSyntax();
            st.lowerCaseMode(true);
            st.wordChars('a', 'z');
            st.wordChars('A', 'Z');
            st.wordChars('0', '9');
            st.wordChars('\'', '\'');
            st.whitespaceChars(0, '&');
            st.whitespaceChars('(', '/');
            st.whitespaceChars(':', '@');
            st.whitespaceChars('[', '`');
            st.whitespaceChars('{', 255);
            boolean inQuote = false;
            int ttype;
            Vector<String> words = new Vector<String>();
            while((ttype = st.nextToken()) != StreamTokenizer.TT_EOF)
            {
                if(ttype == StreamTokenizer.TT_WORD)
                {
                    String s = st.sval;
                    if(!inQuote && s.charAt(0) == '\'')
                    {
                        inQuote = true;
                        s = s.substring(1);
                    }
                    int len = s.length();
                    if(inQuote && len > 0 && s.charAt(len - 1) == '\'')
                    {
                        s = s.substring(0, len - 1);
                        inQuote = false;
                    }
                    words.add(s);
                }
            }
            
            total = words.size();
            LinkedList<Integer> catBefore = new LinkedList<Integer>();
            LinkedList<Integer> categories = new LinkedList<Integer>();
            for(int i = 0; i < total; i++)
            {
                String before = i == 0 ? "" : words.elementAt(i - 1);
                String after = i == total - 1 ? "" : words.elementAt(i + 1);
                String word = words.elementAt(i);
                LinkedList<Rule> rules = dictionary.get(word);
                out.print(word);
                out.print("/");
                if(rules != null)
                {
                    boolean first = true;
                    for(Rule r : rules)
                    {
                        int c = r.classify(before, catBefore, after);
                        if(c != 0)
                        {
                            categories.add(c);
                            if(!first)
                            {
                                out.print('+');
                            }
                            out.print(descriptions.get(c));
                            first = false;
                        }
                    }
                }
                out.print(' ');
                LinkedList<Integer> tmp = categories;
                categories = catBefore;
                catBefore = tmp;
                categories.clear();
            }
            out.println();
        }
    }

    private void calc() throws Exception
    {
        StreamTokenizer st = new StreamTokenizer(new InputStreamReader(in));
        st.lowerCaseMode(true);
        //st.wordChars('a', 'z');
        //st.wordChars('A', 'Z');
        //st.wordChars('0', '9');
        //st.wordChars('\'', '\'');
        //st.whitespaceChars(0, '&');
        //st.whitespaceChars('(', '/');
        //st.whitespaceChars(':', '@');
        //st.whitespaceChars('[', '`');
        //st.whitespaceChars('{', 255);
        st.whitespaceChars(0, 32);
        st.wordChars(33, 126);
        st.whitespaceChars(127, 255);
        st.parseNumbers();
        st.ordinaryChar('.');
        st.ordinaryChar('-');
        boolean inQuote = false;
        int ttype;
        Vector<String> words = new Vector<String>();
        while((ttype = st.nextToken()) != StreamTokenizer.TT_EOF)
        {
            if(ttype == StreamTokenizer.TT_WORD)
            {
                String s = st.sval;
                while (s.length() > 1) {
                    if (!Character.isLetter(s.charAt(0))) {
                        words.add(s.substring(0, 1));
                        s = s.substring(1);
                    } else if (!Character.isLetter(s.charAt(s.length() - 1))) {
                        words.add(s.substring(s.length() - 1));
                        s = s.substring(0, s.length() - 1);
                    } else
                        break;
                }
                words.add(s);
            } else if (ttype == StreamTokenizer.TT_NUMBER) {
                words.add("one");
            } else if (ttype > 0) {
                words.add((new Character((char)ttype)).toString());
            }
        }

        total = words.size();
        LinkedList<Integer> catBefore = new LinkedList<Integer>();
        LinkedList<Integer> categories = new LinkedList<Integer>();
        for(int i = 0; i < total; i++)
        {
            String before = i == 0 ? "" : words.elementAt(i - 1);
            String after = i == total - 1 ? "" : words.elementAt(i + 1);
            String word = words.elementAt(i);
            LinkedList<Rule> rules = dictionary.get(word);
            if(rules == null)
            {
                int[] count = unknowns.get(word);
                if(count == null)
                {
                    count = new int[] {1};
                    unknowns.put(word, count);
                }
                else
                {
                    ++count[0];
                }
            }
            else
            {
                for(Rule r : rules)
                {
                    int c = r.classify(before, catBefore, after);
                    if(c != 0)
                    {
                        categories.add(c);
                        int[] count = counts.get(c);
                        if(count == null)
                        {
                            count = new int[]{1};
                            counts.put(c, count);
                        }
                        else
                        {
                            ++count[0];
                        }
                    }
                }
            }
            LinkedList<Integer> tmp = categories;
            categories = catBefore;
            catBefore = tmp;
            categories.clear();
        }
    }
    
    private void output()
    {
     //   out.println("Total number of words: " + total);
     //   out.println("Categories:");
        for(Entry<Integer, String> entry : descriptions.entrySet())
        {
            int[] count = counts.get(entry.getKey());
            int n = count == null ? 0 : count[0];
    //        out.println("    " + entry.getValue() + ":\t" + n + "\t" + (Math.floor(1000.0 * n / total + 0.5) / 10) + "%");
	      out.println(n);
        }
    //    out.println("Unknown words:");
    //    for(Entry<String, int[]> entry : unknowns.entrySet())
    //    {
    //        out.println("    " + entry.getKey() + ": " + entry.getValue()[0]);
   //     }
    }
    
    private static class Rule
    {
        private static final int TYPE_EXPLICIT = 0;
        private static final int TYPE_CONDITION_BEFORE = 1;
        private static final int TYPE_CONDITION_AFTER = 2;

        private static final Pattern intPattern = Pattern.compile("[0-9]+");

        private int type = -1;
        private Object[] conditions = null;
        private int category = 0;
        private int negCategory = 0;
        
        public Rule(String str) throws Exception
        {
            int index = -1;
            if(intPattern.matcher(str).matches())
            {
                type = TYPE_EXPLICIT;
                category = Integer.valueOf(str);
            }
            else if(str.charAt(0) == '<' && (index = str.indexOf('>')) != -1)
            {
                type = TYPE_CONDITION_AFTER;
                String condition = str.substring(1, index);
                String cat = str.substring(index + 1);
                String[] conds = condition.split("[\\s]+");
                conditions = new Object[conds.length];
                for(int i = 0; i < conds.length; i++)
                {
                    conditions[i] = conds[i];
                }
                String[] cats = cat.split("/");
                category = Integer.valueOf(cats[0].trim());
                if(cats.length == 2)
                {
                    negCategory = Integer.valueOf(cats[1].trim());
                }
                else if(cats.length > 2)
                {
                    throw new Exception();
                }
            }
            else if(str.charAt(0) == '(' && (index = str.indexOf(')')) != -1)
            {
                type = TYPE_CONDITION_BEFORE;
                String condition = str.substring(1, index);
                String cat = str.substring(index + 1);
                String[] conds = condition.split("[\\s]+");
                conditions = new Object[conds.length];
                for(int i = 0; i < conds.length; i++)
                {
                    String s = conds[i];
                    if(intPattern.matcher(s).matches())
                    {
                        conditions[i] = Integer.valueOf(s);
                    }
                    else
                    {
                        conditions[i] = s;
                    }
                }
                String[] cats = cat.split("/");
                category = Integer.valueOf(cats[0].trim());
                if(cats.length == 2)
                {
                    negCategory = Integer.valueOf(cats[1].trim());
                }
                else if(cats.length > 2)
                {
                    throw new Exception();
                }
            }
            else
            {
                throw new Exception();
            }
        }
        
        public int classify(String before, LinkedList<Integer> catBefore, String after)
        {
            int ret = 0;
            switch(type)
            {
            case TYPE_EXPLICIT:
                ret = category;
                break;

            case TYPE_CONDITION_BEFORE:
                ret = matches(before, catBefore) ? category : negCategory;
                break;

            case TYPE_CONDITION_AFTER:
                ret = matches(after, null) ? category : negCategory;
                break;
                
            default:
            }
            return ret;
        }
        
        private boolean matches(String word, LinkedList<Integer> cat)
        {
            for(Object obj : conditions)
            {
                if(obj instanceof Integer)
                {
                    int i = (Integer) obj;
                    for(int c : cat)
                    {
                        if(c == i)
                        {
                            return true;
                        }
                    }
                }
                else if(obj instanceof String)
                {
                    String s = (String) obj;
                    if(word.equals(s))
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
    
    private static class Dictionary
    {
        private HashMap<String, LinkedList<Rule>> map = new HashMap<String, LinkedList<Rule>>();
    
        private Dictionary()
        {
        }
        
        private void put(String str, LinkedList<Rule> rules)
        {
            map.put(str, rules);
        }
        
        private LinkedList<Rule> get(String str)
        {
            LinkedList<Rule> ret = map.get(str);
            int len = str.length();
            while(ret == null && len >= 0)
            {
                String s = str.substring(0, len) + '*';
                ret = map.get(s);
                --len;
            }
            return ret;
        }
    }
}
