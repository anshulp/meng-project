
def processWeights(ngram):
    
    wtlines = open("../data/rate_md/experiments/bigram-processed-weights-fold1").readlines()
    
    wtdict = {}
    
    for line in wtlines:
        sp = line.split(" : ")
        
        wtdict[int(sp[0])] = float(sp[1])
    
    tokenlines = open("../data/rate_md/experiments/bigram-tokenlist.txt").readlines()
    
    tokendict = {}
    
    for line in tokenlines:
        sp = line.split(" ")
        s = ""
        
        i = 1
        
        while i <= ngram:
            s += sp[i] + " "
            i += 1
        
        tokendict[int(sp[0])] = s.strip()
    
    f = open("../data/rate_md/experiments/bigram-weighted-features", "w")
    
    i = 0 
    
    str1 = ""
    n = len(wtdict.items())
    print n/2
    for key, val in sorted([(value,key) for (key,value) in wtdict.items()]):
        if i < 1000:
            str1 = str(tokendict[val]) + ": " + str(key)  + "\n"

            f.write(str1) 
            i += 1
    
    i = 0
    for key, val in sorted([(value,key) for (key,value) in wtdict.items()], reverse=True):
        if i < 1000:
            str1 = str(tokendict[val]) + ": " + str(key)  + "\n"

            f.write(str1) 
            i += 1
    
    
    f.close()        
            


processWeights(2)   
print 'Done !'

