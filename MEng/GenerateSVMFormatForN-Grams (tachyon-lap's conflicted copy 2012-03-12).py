#Author - Anshul Pandey (AP698)
#03/02/2012

import os, re, random


tokenList = []

def findAllTokens(fname):
    
    line = str(open(fname, 'r').readline())
    
    tokens = line.lower().split(" ")

    for i in range(0, len(tokens)):
        token = str(tokens[i])
        key = token.split("/")
        
        try:
            s = ""
            #to take care of 170/night as a single word
            if len(key) > 2:
                i = 0
                while i < len(key)-1:
                    s += key[i]+"/"
                    i += 1
                #remove the last "/"
                s = s[:len(s)-1]
            else:
                s = str(key[0])
            s = s.strip()
            if tokenList.__contains__(s):
                continue
            else:
                tokenList.append(s)
        except:
            continue
        


def getTokenList():
    for dsname in ['MTurk', 'TripAdvisor']:
        for i in range(1, 6):
            dir = '../data/op_spam_v3/' + dsname + '/' + 'fold' + str(i) + '/'
            for filename in os.listdir(dir):
                if filename.endswith('.uni.txt'):
                    fname = dir + filename
                    findAllTokens(fname)




def getLine(fname, tokenList):
    line = str(open(fname, 'r').readline())
    newLine = ""
    i = 0
    for item in tokenList[:]:
        
        token = str(tokenList[i])
#        print token
        if token == "+":
            token = "/+"
        elif token == "?":
            token = "/?"
        l = len(re.findall(token, line.lower(), 0))
        newLine += str(i+1)+":"+str(l)+" "
        i = i+1
   
    return newLine.strip() 

               

def createDatasetForSVMLight(tokenList):
    
    newFile = open("../data/NGram-SVMFile", "a")
    for dsname in ['MTurk', 'TripAdvisor']:
        for i in range(1, 6):
            dir = '../data/op_spam_v3/' + dsname + '/' + 'fold' + str(i) + '/'
            for filename in os.listdir(dir):
                if filename.endswith('.uni.txt'):
                    fname = dir + filename
                    newLine = getLine(fname, tokenList)
                    if filename.startswith('t_'):
                        newFile.write("+1 "+newLine)
                    else:
                        newFile.write("-1 "+newLine)
                    newFile.write("\n")
    newFile.close()



def randomlyCreateTrainAndTestSVMFiles():
    lines = open("../data/NGram-SVMFile").readlines()
    
    f =  open("../data/NGram-SVMFile")
   
    trainLines = []
    
    writeTrainFile = open("../data/NGram-dataSVM.train", "w")
    i = 0
    while i < 650:
        j = random.randint(0, 799)
        if trainLines.__contains__(j):
            continue
        trainLines.append(j)
        
        writeTrainFile.write(lines[j])
        
        i = i+1
        
    writeTrainFile.close()
    
    writeTestFile = open("../data/NGram-dataSVM.test", "w")    
    
    i = 0
    while i < 800:
        if trainLines.__contains__(i):
            i = i+1
            continue
        writeTestFile.write(lines[i])
        i = i + 1
        
    writeTestFile.close()
        
               
          

def __main__():
    
    getTokenList()
    print tokenList, len(tokenList)
    
    if os.path.exists("../data/NGram-SVMFile"):
        os.remove("../data/NGram-SVMFile")
    if os.path.exists("../data/NGram-dataSVM.train"):
        os.remove("../data/NGram-dataSVM.train")
    if os.path.exists("../data/NGram-SdataSVM.test"):
        os.remove("../data/NGram-ataSVM.test")
        
    createDatasetForSVMLight(tokenList)
    
    randomlyCreateTrainAndTestSVMFiles()    
    print "SVM Train and Test Files for N-Gram data are Created"
        
        
__main__()

