import os


def runsvm(numfold):
    
    k = 0
    while k < numfold:
        
        os.system("../../svm_learn -c 0.5 bigram-svmfile-rate-md-fold"+str(k)+".train ./model/bigram-model-fold"+str(k))
        print
        print
        
        os.system("../../svm_classify bigram-svmfile-rate-md-fold"+str(k)+".test ./model/bigram-model-fold"+str(k)+" ./predict/bigram-predict-fold"+str(k))#+" > ./result/bigram-result-fold"+str(k))
        
        os.system("python ../../svm2weights.py ./model/bigram-model-fold"+str(k)+" > ./result/bigram-processed-weights-fold"+str(k))
        k += 1
        
    
    
runsvm(5)

print 'done !'