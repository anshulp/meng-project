#Author - Anshul Pandey (AP698)
#03/02/2012

import os, re, random

tagsFreq = {}
tagsList = []

#def initializeTagList():
#    for line in open("../data/Tags.txt", "r").readlines():
#        s = line.split("\t")[1]
#        tagsList.append(s)
       

def findAllTags(fname):
    
    line = str(open(fname, 'r').readline())
    
    tokens = line.split(" ")
#    print len(tokens)
    for i in range(0, len(tokens)):
        token = str(tokens[i])
        key = token.split("/")
       
        try:
            s = str(key[len(key) - 1])
            s = s.strip()
            if tagsFreq.has_key(s):
                tagsFreq[s] = tagsFreq[s] + 1
            else:
                tagsFreq[s] = 1
        except:
            continue
        
        
        
def getLine(fname, tagsList):
    line = str(open(fname, 'r').readline())
    newLine = ""
    i = 0
    for item in tagsList[:]:
        
        token = str(tagsList[i])
        
        l = len(re.findall(token, line, 0))
        newLine += str(i+1)+":"+str(l)+" "
        i = i+1
   
    return newLine.strip()            
    
    
    
def findTags():
    for dsname in ['MTurk', 'TripAdvisor']:
        for i in range(1, 6):
            dir = '../data/op_spam_v3/' + dsname + '/' + 'fold' + str(i) + '/'
            for filename in os.listdir(dir):
                if filename.endswith('.uni.txt'):
                    fname = dir + filename
                    findAllTags(fname)
                   
                   

def createDatasetForSVMLight(tagsList):
    
    newFile = open("../data/POS-SVMFile", "a")
    for dsname in ['MTurk', 'TripAdvisor']:
        for i in range(1, 6):
            dir = '../data/op_spam_v3/' + dsname + '/' + 'fold' + str(i) + '/'
            for filename in os.listdir(dir):
                if filename.endswith('.uni.txt'):
                    fname = dir + filename
                    newLine = getLine(fname, tagsList)
                    if filename.startswith('t_'):
                        newFile.write("+1 "+newLine)
                    else:
                        newFile.write("-1 "+newLine)
                    newFile.write("\n")
    newFile.close()
   


def randomlyCreateTrainAndTestSVMFiles():
    lines = open("../data/POS-SVMFile").readlines()
    
    trainLines = []
    
    writeTrainFile = open("../data/POSdataSVM.train", "w")
    i = 0
    while i < 650:
        j = random.randint(0, 799)
        if trainLines.__contains__(j):
            continue
        trainLines.append(j)
        
        writeTrainFile.write(lines[j])
        
        i = i+1
        
    writeTrainFile.close()
    
    writeTestFile = open("../data/POSdataSVM.test", "w")    
    
    i = 0
    while i < 800:
        if trainLines.__contains__(i):
            i = i+1
            continue
        writeTestFile.write(lines[i])
        i = i + 1
        
    writeTestFile.close()
        
        
    
    

def __main__():
    
    findTags()                
    tagsList = tagsFreq.keys()
    
    if os.path.exists("../data/POS-SVMFile"):
        os.remove("../data/POS-SVMFile")
    if os.path.exists("../data/POSdataSVM.train"):
        os.remove("../data/POSdataSVM.train")
    if os.path.exists("../data/POSdataSVM.test"):
        os.remove("../data/POSdataSVM.test")
        
    createDatasetForSVMLight(tagsList)
    
    randomlyCreateTrainAndTestSVMFiles()    
    print "SVM Train and Test Files Created"
        
        
__main__()

 

