import os, re, random, operator


N = 5


def getcommondocs():
    
    docs = open(path+"docs-till-now.txt").readlines()
    
    for doc in docs:
        
        count_true = 0
        count_mturk = 0
        count = 0

        for dsname in dsnames:
            dir = path + dsname + "/" + doc.strip() + "/"
            if os.path.exists(dir):
                if dsname == 'true':
                    count_true = len(os.listdir(dir)) 
                elif dsname == 'mturk':
                    count_mturk = len(os.listdir(dir)) 
            
        if count_true > count_mturk:
            count = count_mturk
        else:
            count = count_true
                
        if count > 0:
            common_docs[doc.strip()] = count
       
    


def findAllTokens(fname, gram):
    
    line = str(open(fname, 'r').readline())
    
    tokens = line.strip().lower().split(" ")  #re.split('(\w+)',line.strip().lower())
    
    returntokens = []
    
    if gram == 1:
        returntokens = tokens
    elif gram == 2:
        bigramtokens = []
        
        i = 0
        while i+1 < len(tokens):
            t = tokens[i] + " " + tokens[i+1]
            bigramtokens.append(t)
            i += 1
        
        returntokens = bigramtokens
    
    for token in returntokens:
        if tokencount_dict.has_key(token):
            tokencount_dict[token] = tokencount_dict[token] + 1
        else:
            tokencount_dict[token] = 1    
    
    return returntokens

    
def gettokenlist(dsnames, gram):
    
    tokenlist = []
    
    for doc, count in common_docs.items():
        
        for dsname in dsnames:
            i = 0
            dir = path + dsname + "/" + doc.strip() + "/"
           
            if os.path.exists(dir):
                for filename in os.listdir(dir):
                    if i < count and filename.endswith('.txt'):
                        fname = dir + filename
                        tokenlist.extend(findAllTokens(fname, gram))
                        i += 1
    
    return sorted(list(set(tokenlist)))        
                  


def getstoplist(tokenlist):
    
    stoplist = []
    
    sorted_tokencount_dict = sorted(tokencount_dict.iteritems(), key=operator.itemgetter(1), reverse=True)
    
    i = 0
    
    for val in sorted_tokencount_dict:
        if i < len(sorted_tokencount_dict)/N:
            stoplist.append(val[0])
        else:
            break
        i += 1
        
    return stoplist 



           
def getLine(fname, tokenlist, gram):
    
    newLine = ""
    i = 0
    
    linetokens = findAllTokens(fname, gram)
    
    for token in tokenlist:        
        l = linetokens.count(token)
        if l > 0:
            newLine += str(i+1)+":"+str(l)+" "
        i = i+1
    
    return newLine.strip() 

               

def createDatasetForSVMLight(dsnames, tokenlist, gram):
    
    newFile = open(path+"experiments/bigram-svmfile-rate-md", "w")
    
    k = 1
    
    docs = open(path+"docs-till-now.txt").readlines()
    
    for doc, count in common_docs.items():
               
        for dsname in dsnames:
            i = 0
            dir = path + dsname + "/" + doc.strip() + "/"
           
            if os.path.exists(dir):
                for filename in os.listdir(dir):
                    if i < count and filename.endswith('.txt'):
                        fname = dir + filename
                        newLine = getLine(fname, tokenlist, gram)
                        if dsname == 'true':
                            newFile.write("+1 "+newLine)
                        else:
                            newFile.write("-1 "+newLine)    
                        newFile.write("\n")
                        print k, fname
                        k += 1
                        i += 1
                    
                        
                    
    newFile.close()



def randomlyCreateTrainAndTestsvmfiles():
    
    lines = open(path+"experiments/bigram-svmfile-rate-md").readlines()
    
    trainLines = []
    
    writeTrainFile = open(path+"experiments/bigram-svmfile-rate-md.train", "w")
    i = 0
    
    trainnum = len(lines)*0.7
    testnum = len(lines) - trainnum
    
    while i < trainnum:
        j = random.randint(0, len(lines)-1)
        if trainLines.__contains__(j):
            continue
        trainLines.append(j)
        
        writeTrainFile.write(lines[j])
        
        i = i+1
        
    writeTrainFile.close()
    
    writeTestFile = open(path+"experiments/bigram-svmfile-rate-md.test", "w")    
    
    i = 0
    while i < len(lines):
        if trainLines.__contains__(i):
            i = i+1
            continue
        writeTestFile.write(lines[i])
        i = i + 1
        
    writeTestFile.close()



def createSVMFilebyFold(filelist, type, tokenlist, gram, fold):
    
    newFile = open(path+"experiments/bigram-svmfile-rate-md-fold"+str(fold)+"."+type, "w")
    
    k = 0
    
    for fname in filelist:
        newLine = getLine(fname, tokenlist, gram)
        if fname.find("true") != -1:
            newFile.write("+1 "+newLine)
            print "+1"
        else:
            newFile.write("-1 "+newLine)   
            print "-1"
            
        newFile.write("\n")
#        print k, fname
        k += 1
       
    
    
    
    


def getReviewFileList(docs, dsnames):
    
    filelist = []
    
    for doc in docs:                   
            for dsname in dsnames:
                i = 0
                dir = path + dsname + "/" + doc.strip() + "/"
                count = common_docs[doc.strip()]
                if os.path.exists(dir):
                    for filename in os.listdir(dir):
                        if i < count and filename.endswith('.txt'):
                            fname = dir + filename
                            filelist.append(fname)
                            
    return filelist



def createcrossvalidationfiles(dsnames, tokenlist, gram):
    
    k = 0
    
    n = len(common_docs)
    
    docs = list(common_docs.keys())
    
    i = 0
    
    while k < n:
        
        testdocs = []
        testdocs.append(docs[k])
        testdocs.append(docs[k+1])
        testdocs.append(docs[k+2])
        
        filelist = getReviewFileList(testdocs, dsnames)
        
        createSVMFilebyFold(filelist, 'test', tokenlist, gram, i)
        print 'Fold '+str(i), 'test done'
        
        traindocs = list(set(docs).difference(set(testdocs)))
        
        filelist = getReviewFileList(traindocs, dsnames)
        
        createSVMFilebyFold(filelist, 'train', tokenlist, gram, i)
        print 'Fold '+str(i), 'train done'
        
        i += 1
        k += 3
       
    




def writeTokenlistToFile(tokenlist):
    
    f = open(path+"experiments/bigram-tokenlist.txt", 'w')
    i = 1
    for token in tokenlist:
        s = str(i)+" "+token
        f.write(s+"\n")
        i += 1
    f.close()







if __name__ == '__main__':
    
    path = "../data/rate_md/"
    
    tokencount_dict = {}
    common_docs = {}
    
    #uni gram
    gram = 2 
    
    dsnames = ['true', 'mturk'] 
    
    getcommondocs()
    print 'common docs done !'
    
    tokenlist = gettokenlist(dsnames, gram) 
    
    stopwords = getstoplist(tokenlist)
    
    tokenlist = list(set(tokenlist).difference(stopwords))
    
    print 'tokenlist done !'
     
    writeTokenlistToFile(tokenlist) 
    
    if os.path.exists(path+"experiments/bigram-svmfile-rate-md"):  
        os.remove(path+"experiments/bigram-svmfile-rate-md")
    
#    createDatasetForSVMLight(dsnames, tokenlist, gram, i)

    createcrossvalidationfiles(dsnames, tokenlist, gram)
    
    print "SVM File for uni-Gram for rate-md-data are Created" 
    
#    randomlyCreateTrainAndTestsvmfiles()
    print 'Random Train and Test files created'   
        


