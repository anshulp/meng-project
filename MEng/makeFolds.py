import sys
def makeTrainTest(fold,numberOfFold,model):
    filename=[]
    for i in range(numberOfFold):
        filename.append('0')
    for counter in range(numberOfFold):
        filename[counter]=open('../data/'+str(model)+'-SVM.txt'+str(counter),'r')
    trainfile=open('../data/'+str(model)+'-SVMtrain'+str(fold)+'.txt','w')
    testfile=open('../data/'+str(model)+'-SVMtest'+str(fold)+'.txt','w')
    for i in range(numberOfFold):
        if i==fold:
            for line in filename[i]:
                testfile.write(line)
        else:
            for line in filename[i]:
                trainfile.write(line)

makeTrainTest(int(sys.argv[1]),int(sys.argv[2]),sys.argv[3])
print 'done making train'
