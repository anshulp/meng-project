import os, random, math



def generateFeatureVectorForEachComment():
   
    if os.path.exists("./out"):
        os.system("rm -rf ./out")
    os.system("mkdir out")
    
    dir = ""
    j=1
    for dsname in ['MTurk', 'TripAdvisor']:
        for i in range(1, 6):
            dir = '../data/op_spam_v1.3/' + dsname + '/' + 'fold' + str(i) + '/'
            for filename in os.listdir(dir):
#                print filename
                os.system("java LIWC -in "+str(dir+filename)+" -out "+ "./out/out"+str(j) +" -dic LIWC2007_English080130.dic")
                j += 1
#    print j 
   
   
    

def combineFeatureVectors():
  
    i = 1
    str1 = ""
    while i <= 800:
        str1 += "./out/out"+str(i)+" "
        i += 1
#    str1 = str1.strip()
    os.system("paste "+str1.strip()+" > ../data/liwcsvmdata")
#    print j
    
    f = open("../data/liwcactualsvmdata", 'w')
    for c in zip(*(l.split() for l in open("../data/liwcsvmdata").readlines() if l.strip())):
        f.write(' '.join(c))
	f.write("\n")
    f.close()


def normalizeColumns(arr, r, c):
#    print arr
    sarr = []
    for j in range(0, c):
        sum = 0
        for i in range(0, r):
#            print i, j
            sum += pow(arr[i][j], 2)
#        print sum, j
        sarr.append(pow(sum, 0.5))
    print sarr
    #now normalize arr
    for j in range(0, c):
        for i in range(0, r):
            if not sarr[j] == 0:
                arr[i][j] = (arr[i][j] / sarr[j])
    return arr





def normalizeAndCreateSVMData():
    
    r = 800
    c = 76
    arr = [[0.0]*c]*r
    newarr = [[0.0]*c]*r
    f = open("../data/liwcactualsvmdata", 'r')
    lines = f.readlines()
    
#    print len(arr[0])
    j = 0
    i = 0
    for line in lines:
        sp = line.split(" ")
	#print sp	
	j = 0        
	for j in range(0, c):
            arr[i][j] = float(sp[j])
        i += 1
#    newarr = normalizeColumns(arr, r, c)
    print arr

    sarr = []
    i = 0
    j = 0
    for j in range(0, c):
        sum = 0
        for i in range(0, r):
#            print i, j
            sum += pow(arr[i][j], 2)
#        print sum, j
        #sarr.append(pow(sum, 0.5))
        sq = math.sqrt(sum)
	#print sum, sq
	sarr.append(sq)
    print len(sarr), sarr
    filehandle = open("arr", 'w')
    filehandle.write(str(arr))
    filehandle.close()
    
    #now normalize arr
    m = 0
    n = 0
    for m in range(0, r):
	for n in range(0, c):
            if sarr[n] == 0.0:
                newarr[m][n] = 0.0
            else:
                newarr[m][n] = (arr[m][n] / sarr[n])
	    print arr[m][n], sarr[n], newarr[m][n]
#                if newarr[i][j] != 0.0:
#                    print newarr[i][j], i, j
#                    exit(1)

#    print newarr
    f = open("../data/liwcSVMFinalData", 'w')
    
    for i in range(0, r):
        line = ""
        if i < 400:
            line += "+1 "
        else:
            line += "-1 "
        for j in range(0, c):
            line += str(j+1)+":"+str(newarr[i][j])+" "
	    #line += str(j+1)+":"+str(arr[i][j])+" "
        f.write(line.strip())
        f.write('\n')
    
    f.close()
    
    



def randomlyCreateTrainAndTestSVMFiles():
    lines = open("../data/liwcSVMFinalData").readlines()
    #lines = open("../data/liwcactualsvmdata").readlines()
    trainLines = []
    
    writeTrainFile = open("../data/liwcDataSVM.train", "w")
    i = 0
    while i < 650:
        j = random.randint(0, 799)
        if trainLines.__contains__(j):
            continue
        trainLines.append(j)
        
        writeTrainFile.write(lines[j])
        
        i = i+1
        
    writeTrainFile.close()
    
    writeTestFile = open("../data/liwcDataSVM.test", "w")    
    
    i = 0
    while i < 800:
        if trainLines.__contains__(i):
            i = i+1
            continue
        writeTestFile.write(lines[i])
        i = i + 1
        
    writeTestFile.close()
        
        
       
    
    
    
    

if __name__ == '__main__':
    
    #generateFeatureVectorForEachComment()

    combineFeatureVectors()
    
    normalizeAndCreateSVMData()
    randomlyCreateTrainAndTestSVMFiles()
    
    
