#!/bin/bash

python knn.py 1  > ../data/knnoutput_1 
python knn.py 3 > ../data/knnoutput_3 
python knn.py 5 > ../data/knnoutput_5 
python knn.py 10 > ../data/knnoutput_10 
python knn.py 17 > ../data/knnoutput_17 
python knn.py 25 > ../data/knnoutput_25 
python knn.py 50 > ../data/knnoutput_50
