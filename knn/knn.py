'''
author - anshul pandey (ap698)
dated - 03/30/2012
'''

import string
import math
import sys


def get_values(line):
    words = string.split(line)
       
    label = words[0]
    x1 = words[1:N+1]    
    return label,x1


def normalize_trdata(dataset,avg,std, N):    
    f = open('../data/normalized_k_'+k+'trdata', 'w')    
#    normdataset = [];  
    for line in dataset:
        label,instance = get_values(line)
        point =  extract_attrs(instance)
        newlist = []
        str1 = str(label) +" "
        for i in range(N):
            str1 += str(i+1)+':'+str((float(point[i])-avg[i])/std[i]) + " "
        str1 += '\n'
        f.write(str1)
    f.close()
    ntrdataset = open('../data/normalized_k_'+k+'trdata').readlines()
    return ntrdataset
    
    
def normalize_tstdata(dataset,avg,std, N):
    f = open('../data/normalized_k_'+k+'tstdata', 'w')    
    for line in dataset:
        label,instance = get_values(line)
        point =  extract_attrs(instance)
        newlist = []
        str1 = str(label) + " "
        for i in range(N):
            str1 += str(i+1)+':'+str((float(point[i])-avg[i])/std[i]) + " "
        str1 += '\n'
        f.write(str1)
    f.close()
    ntstdataset = open('../data/normalized_k_'+k+'tstdata').readlines()
    return ntstdataset
    

def normalize(trdataset,tstdataset, N):
    avg = []
    sum = []
    std = []
    x1  = []
    for i in range(N):
        sum.append(0)
        avg.append(0)
        std.append(0)
        
    #finding average
    for line in trdataset:
        label,point = get_values(line)
        x1.append(point)
        attr = extract_attrs(point)
        for i in range(N):
            sum[i] += float(attr[i])
    print sum
        
    for i in range(N):
        avg[i] =  sum[i]/len(trdataset)
    print avg
    
    #finding standard deviation
    for line in trdataset:
        label,instance = get_values(line)
        point =  extract_attrs(instance)
        for i in range(N):
            std[i] += math.pow((float(point[i]) - avg[i]),2)
    for i in range(N):
        std[i] = math.sqrt(std[i]/len(trdataset))    
#    print std
    ntrdata = normalize_trdata(trdataset,avg,std, N)
    ntstdata = normalize_tstdata(tstdataset,avg,std, N)
    return ntrdata,ntstdata



def extract_attrs(x1):
    attr_values = []
    for attr in x1:
        attr_values.append(string.split(attr,':')[1])
    return attr_values

def euclidean_distance(x1attrs,x2attrs, N):
    sum = 0
    for i in range(N):
        sum += math.pow(float(x1attrs[i]) - float(x2attrs[i]),2)
    ed = math.sqrt(sum)
#    print ed
    return ed

def find_nearest(x1,trdataset,tstdataset, N):
    sim = []
    dist = {}
    predicted_label = 0
    for line in trdataset:
        words = string.split(line, ' ')
        label = words[0]
        x1attrs = extract_attrs(x1)
        
        x2attrs = extract_attrs(words[1:N+1])
        eu_dist = euclidean_distance(x1attrs,x2attrs, N)
        sim.append(eu_dist)
        dist[eu_dist]= float(label)
    if weighted == 'false':
        for i in range(k):
            min_dist = min(sim)
            predicted_label += dist[min_dist]
            sim.remove(min_dist)
        predicted_label = predicted_label/float(k)
    else:
        similarity_sum = 0
        for i in range(k):
            min_dist = min(sim)
            predicted_label += dist[min_dist]*math.pow(math.e, -min_dist)
            similarity_sum +=  math.pow(math.e, -min_dist)
            sim.remove(min_dist)
        predicted_label = predicted_label /similarity_sum
#   print predicted_label
    return float(predicted_label)
        
              
def report_rmse(trdataset,tstdataset, N):
    error = 0

    for line in tstdataset:
        label,x1 = get_values(line)
#        print label
        predicted_label = find_nearest(x1,trdataset,tstdataset, N)
        print label, ('+1' if predicted_label > 0.0 else '-1'), "#", predicted_label
        error += math.pow((float(label) - predicted_label),2)
    rmse = math.sqrt(error/len(tstdataset))   
    print rmse
    

def init(trfile,tstfile, normalized):
    trdataset = open(trfile).readlines()
    tstdataset = open(tstfile).readlines()
    N = len(tstdataset[0].split(" ")) -  1
    if normalized == 'true':
        trdataset,tstdataset = normalize(trdataset,tstdataset, N)       

    return trdataset,tstdataset, N
   
    
if __name__ == '__main__':
        
    #training_file,test_file,k,weighted,normalized = sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4],sys.argv[5]
    k = int(sys.argv[1]) 
    tr_file = "../data/NGram-dataSVM.train"
    tst_file = "../data/NGram-dataSVM.test"
    weighted = 'false'
    
    trdataset,tstdataset, N = init(tr_file,tst_file, 'false')

    report_rmse(trdataset,tstdataset, N)


#normalize()
#report_rmse()
